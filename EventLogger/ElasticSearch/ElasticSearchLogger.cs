﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Configuration;

namespace DataReader
{
	public class ElasticSearchLogger
	{
		public  Uri node;
		public  ConnectionSettings settings;
		public  ElasticClient client;

		public ElasticSearchLogger() {

			node = new Uri(ConfigurationManager.AppSettings["elasticsearchserverurl"]);
			settings = new ConnectionSettings(node).DefaultIndex(ConfigurationManager.AppSettings["indexname"]);
			client = new ElasticClient(settings);
		}

		public void logMessage(Event eventData) {
			client.Index(eventData);
		}

		public  Event lastEventLogEntryDateTime()
		{
			var result =
			client.Search<Event>(s => s
				.MatchAll()
				.Size(1)
				.Sort(sr => sr
					.Field("createdDate", SortOrder.Descending)
				));

			return result.Documents.Count() > 0 ? result.Documents.FirstOrDefault() : null;
		}

	}
}
