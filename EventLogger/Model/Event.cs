﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataReader
{
	public class Event
	{
		public string WorkStation { get; set; }
		public DateTime CreatedDate { get; set; }
		public string EventText { get; set; }
		public int EventID { get; set; }
		public string Action { get; set; }
	}
}
