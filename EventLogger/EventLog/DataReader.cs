﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Diagnostics.Eventing.Reader;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.IO;
using System.Xml;

namespace DataReader
{
	
	public class EventReader
	{
		#region private member
		System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog();
		private EventLog _log;
		private Object thisLock = new Object();
		#endregion

		public EventReader(EventLog log)
		{
			_log = log;
		}

		/**
		 * Reads data from windwos Event logging system and log into elastic search db
		 **/
		public void readData()
		{
			eventLog.Log = "Security";
			var fromDate = DateTime.Now.AddHours(-1);
			
			ElasticSearchLogger loger = new ElasticSearchLogger();
			// lock the code so only one thread gets executed
			lock (thisLock)
			{
				var lastEvent = loger.lastEventLogEntryDateTime();
				if (lastEvent != null)
				{
					fromDate = lastEvent.CreatedDate;
				}
				string taskId = ConfigurationManager.AppSettings["taskId"];
				string eventIds = createEventIdsQuery();
				string queryString = String.Format("*[System[({0}) and Task={2} and TimeCreated[@SystemTime > '{1}']]]", eventIds, fromDate.ToUniversalTime().ToString("o"), taskId);
				Filter modifyFilter = JsonConvert.DeserializeObject<Filter>(ConfigurationManager.AppSettings["modifyFilter"].ToString());
				Filter deleteFilter = JsonConvert.DeserializeObject<Filter>(ConfigurationManager.AppSettings["deleteFilter"].ToString());
		
				try
				{
					EventLogQuery eventsQuery = new EventLogQuery("Security", PathType.LogName, queryString);
					EventLogReader logReader = new EventLogReader(eventsQuery);
					for (EventRecord eventdetail = logReader.ReadEvent(); eventdetail != null; eventdetail = logReader.ReadEvent())
					{

						// Read Event details
						string data = eventdetail.ToXml();
						if (applyFilter(eventdetail, modifyFilter)) {
							loger.logMessage(createDTO(eventdetail, "Modify"));
						}
							
						if (applyFilter(eventdetail, deleteFilter))
						{
							loger.logMessage(createDTO(eventdetail, "Delete"));
						}
						
					}
				}
				catch (EventLogNotFoundException e)
				{
					_log.WriteEntry(string.Format("Error: A problem with EventLogger: Current time is: {0}.", DateTime.Now.ToString("HH:mm:ss") + e.Message), EventLogEntryType.Error);
					Console.WriteLine("Error while reading the event logs" + e.Message);
					return;
				}
			}
		}

		public bool applyFilter(EventRecord eventdetail, Filter filter)
		{ 
			bool elementFound = false;
			
			foreach (EventProperty prop in eventdetail.Properties) {

				string[] split = filter.value.Split(' ');
				foreach (string str in split) {
					if (filter.id.Equals(eventdetail.Id.ToString()) && prop.Value.ToString().Contains(str))
					{
						elementFound = true;
					}
					else {
						elementFound =  false;
						break;
					}
				}
				if (elementFound) return true;
			}

			return elementFound;
		}

		/**
		 * Extracts Event IDs from App.config file and builds the query for Event Veiwe
		 * 
		 */
		private string createEventIdsQuery() {
			StringBuilder eventIds = new StringBuilder();
			var str = ConfigurationManager.AppSettings["eventids"].Split(',');
			bool isFristItemPassed = false;

			foreach(string a in str){
				eventIds.Append((isFristItemPassed ?" or ": "")  + String.Format("EventID={0}", a));
				isFristItemPassed = true;
			}
			return eventIds.ToString();
		}

		/**
		 * Creates DTO for elastic search DB
		 * 
		 **/
		public static Event createDTO(EventRecord record, string action)
		{
			return new Event {
				WorkStation =record.MachineName,
				CreatedDate =(DateTime) record.TimeCreated,
				EventText = record.ToXml(),
				EventID = record.Id,
				Action = action
			};
		}
	}

	public class Filter {

		public String id { get; set; }
		public String value { get; set; }
	}
}
