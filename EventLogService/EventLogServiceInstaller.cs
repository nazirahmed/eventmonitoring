﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace EventLogService
{
	[RunInstaller(true)]
	public class EventLogServiceInstaller : Installer
	{
		public EventLogServiceInstaller()
		{
			ServiceProcessInstaller spi = new ServiceProcessInstaller();
			spi.Account = ServiceAccount.LocalSystem;

			ServiceInstaller si = new ServiceInstaller();
			si.ServiceName = ConfigurationManager.AppSettings["ServiceName"];
			si.StartType = ServiceStartMode.Automatic;
			this.Installers.Add(spi);
			this.Installers.Add(si);
		}
	}
}
