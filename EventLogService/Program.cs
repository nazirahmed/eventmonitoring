﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EventLogService
{
	static class Program
	{
		[DllImport("kernel32.dll")]
		public static extern Boolean AllocConsole();

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
			if (args.Length > 0)
			{
				switch (args[0])
				{
					case "-console":
						AllocConsole();

						EventLoggerApp app = new EventLoggerApp();
						app.Start();

						string input = string.Empty;
						Console.Write("Eventlogger Console started. Type 'exit' to stop the application: ");

						// Wait for the user to exit the application
						while (input.ToLower() != "exit") input = Console.ReadLine();

						// Stop the application.
						app.Stop();
						break;
					case "-install":
						try
						{
							System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
						}
						catch (Exception ex)
						{
							System.Console.WriteLine(ex.ToString());
						}
						break;
					case "-start":
						//starts the service
						try
						{
							ServiceController service = new ServiceController(ConfigurationManager.AppSettings["ServiceName"]);
							service.Start();
						}
						catch (Exception ex)
						{
							System.Console.WriteLine(ex.ToString());
						}
						break;
					case "-stop":
						//stops the service
						try
						{
							ServiceController service2 = new ServiceController(ConfigurationManager.AppSettings["ServiceName"]);
							if (service2.CanStop)
							{
								service2.Stop();
							}
						}
						catch (Exception ex)
						{
							System.Console.WriteLine(ex.ToString());
						}
						break;
					case "-run":
						//starts the system as a debugable console app
						new EventLoggerService().RunConsole();
						break;
					case "-uninstall":
						try
						{
							System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
						}
						catch (Exception ex)
						{
							System.Console.WriteLine(ex.ToString());
						}
						break;
					default:
						System.Console.WriteLine("No action is performed");
						break;
				}
			}
			else
			{
				ServiceBase[] ServicesToRun;
				ServicesToRun = new ServiceBase[] { new EventLoggerService() };
				ServiceBase.Run(ServicesToRun);
			}
		}
	}
}
