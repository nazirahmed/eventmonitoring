﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DataReader;

namespace EventLogService
{
	public class EventLoggerApp
	{
		
		#region Member fields
		// Private fields
		private Thread _thread;
		private EventLog _log;
		EventReader reader;
		#endregion Member fields

		#region Private methods
		private void Execute()
		{
			int pollInterval = Int32.Parse(ConfigurationManager.AppSettings["pollinterval"]);
			// Loop until the thread gets aborted
			try
			{
				while (true)
				{
					reader = new EventReader(_log);
					reader.readData();
					// Sleep for two minute 
					Thread.Sleep(60000 * pollInterval);
				}
			}                                     
			catch (ThreadAbortException)
			{
				_log.WriteEntry("INFO (EventLoggerApp.Execute): Thread aborted.");
			}
		}
		#endregion Private methods

		#region Public methods
		public void Start()
		{
			// Check if the EventLoggerService Event Log Source exists, when not
			// create it
			if (!EventLog.SourceExists("EventLoggerSource"))
				EventLog.CreateEventSource("EventLoggerSource", "Event Logger");

			_log = new EventLog();
			_log.Source = "EventLoggerSource";


			_thread = new Thread(new ThreadStart(Execute));
			_thread.Start();
		}

		public void Stop()
		{
			if (_thread != null)
			{
				_thread.Abort();
				_thread.Join();
			}
		}
		#endregion Public methods
	}
}
